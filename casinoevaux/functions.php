<?php
require_once get_template_directory().'/core/ThemeBuilder.php';

function get_theme_builder(){
    return ThemeBuilder::create_builder();
}

function init_theme(){
    $builder = get_theme_builder();
    $builder->init_theme();
    $builder->add_bootstrap_shortcode();
    $builder->create_option_page();
}
add_action('after_setup_theme','init_theme');


function get_theme_logo(){
    $builder = get_theme_builder();
    return $builder->get_theme_logo();
}

function import_theme_styles(){
    $style_list = array(
        'main-style' => array(
            'src' => 'casinoevaux.css'
        ),
    );
    $builder = get_theme_builder();
    $builder->import_google_fonts(array('Arvo|Open+Sans'));
    $builder->import_assets_style($style_list);
}
add_action("wp_enqueue_scripts","import_theme_styles");

function import_theme_scripts(){
    $script_list = array(
        'vendor-js' => array(
            'src' => 'casinoevaux-vendor.js'
        ),
        'main-js' => array(
            'src' => 'casinoevaux.js'
        ),
        
    );
    $builder = get_theme_builder();
    $builder->import_assets_script($script_list);
}
add_action("wp_enqueue_scripts","import_theme_scripts");

function get_page_slider_data(){
   $builder = get_theme_builder();
   return $builder->get_slider_data('website_slider','slider',array('link'));
}

function has_sidebar_widget(){
    $builder = get_theme_builder();
    return $builder->has_sidbar_widget();
}

function body_classes( $classes ) {
    // Adds a class of custom-background-image to sites with a custom background image.
    if ( get_background_image() ) {
        $classes[] = 'custom-background-image';
    }

    // Adds a class of group-blog to sites with more than 1 published author.
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of no-sidebar to sites without active sidebar.
   
    if ( is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'has-sidebar';
    } else {
        $classes[] = 'no-sidebar';
    }

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    return $classes;
}
add_filter( 'body_class', 'body_classes' );