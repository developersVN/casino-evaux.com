<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="evaux-header" id="evaux-header">
        <nav class="navbar">
            <div class="container">
                <div class="rows">
                    <div class="navbar-header">
                        <button class="navbar-toggle" id="menu-button" data-toggle="collapse" data-target="#evaux-menu-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span><span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                        <a class="navbar-brand logo" href="<?php echo home_url();?>">
                        <img src="<?php echo get_theme_logo(); ?>" alt="<?php bloginfo('name'); ?>" />
                 </a>
                    
                    </div>
                    <div class="collapse navbar-collapse" id="evaux-menu-nav">
                        <?php
                        wp_nav_menu( array(
                            'menu' => 'top_menu',
                            'theme_location' => 'primary',
                            'depth' => 2,
                            'container' => false,
                            'menu_class' => 'nav navbar-nav',
                            //Process nav menu using our custom nav walker
                            'walker' => new wp_bootstrap_navwalker() ) );
                        ?>
                        <button class="search-toggle" id="search-button" data-toggle="collapse" data-target="#evaux-search"></button>
                        <div id="evaux-search" class="evaux-search-form">
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                    <?php $header_image = get_header_image(); if($header_image) : ?>
                    <div style="background-image: url(<?php echo $header_image ?>)" class="evaux-banner">
                        <img src="<?php echo $header_image ?>" alt="evaux-banner" class="banner-image">
                    </div>
                    <?php endif; ?>
                </div>
                
            </div>
        </nav>
    </header>

    <div class="container evaux-base-container">