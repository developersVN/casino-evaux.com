<?php
/**
 *
 * This is the template that displays all pages by default.
 *
 */
get_header(); ?>
    <div id="evaux-primary" class="evaux-content-area">
       	<main id="evaux-main" class="evaux-site-main" >
            
            <div class="evaux-page-title">
                <h1><?php the_title();?></h1>
            </div>
            <?php
                //Get page content
                // Start the loop.
                while ( have_posts() ) : the_post();
            ?>
                    <div class="evaux-the-content">
                        <?php
                            the_content();
                        ?>
                    </div>
            <?php
            // End the loop.
            endwhile;
            ?>
        </main><!-- .site-main -->
    </div><!-- .content-area -->
<?php get_footer(); ?>